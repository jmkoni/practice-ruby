def sum(array)
    sum = 0
    array.each { |num|
        sum += num if num.is_a? Integer
    }
    sum
end

def max_2_sum(array)
    sum = 0
    if array.length <= 2
        array.each { |num|
            sum += num if num.is_a? Integer
        }
    else
        array.sort! {|a,b| b <=> a}
        puts array
        sum = array[0] + array[1]
    end
    sum
end

def sum_to_n?(array,n)
    array.sort!
    a = 0 # start of array
    z = array.length - 1 #end of array
    if array.length < 1 && n == 0
        return true
    end
    
    while a < z
        temp_sum = array[a] + array[z]
        if temp_sum == n
            return true
        elsif temp_sum > n
            z -= 1
        else
            a += 1
        end
    end
    return false
end
