def hello(name)
    "Hello, " + name
end

def starts_with_consonant?(s)
    first = s[0]
    return !(/[b-df-hj-np-tv-z]/i.match(first)).nil?
end

def binary_multiple_of_4?(s)
    if (/^[0-1]+$/.match(s)).nil?
        return false
    elsif s.length <= 2
        return false
    elsif (s[-1] == "0") && (s[-2] == "0")
        return true
    else
        return false
    end
end
