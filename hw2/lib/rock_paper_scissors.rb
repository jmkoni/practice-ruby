class RockPaperScissors

  # Exceptions this class can raise:
  class NoSuchStrategyError < StandardError ; end

  def self.name_to_number(name)
    if name == "R"
        number = 0
    elsif name == "P"
        number = 1
    elsif name == "S"
        number = 2
    else
        raise(NoSuchStrategyError, "Strategy must be one of R,P,S")
    end
    return number
  end

  def self.winner(player1, player2)
    player1_num = name_to_number(player1[1])
    player2_num = name_to_number(player2[1])
    diff = (player1_num - player2_num) % 3
    if diff == 0
      return player1
    elsif (diff == 1)
      return player1
    elsif (diff == 2)
      return player2
    else
      raise(NoSuchStrategyError,"Strategy must be one of R,P,S")
    end
  end

  def self.tournament_winner(tournament)
    if tournament[0][0].is_a? String
        return winner(tournament[0],tournament[1])
    end
    # Otherwise keep going down the rabbit hole
    return tournament_winner([tournament_winner(tournament[0]),tournament_winner(tournament[1])])
  end

end
