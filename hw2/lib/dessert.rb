class Dessert
  def initialize(name, calories)
    # Instance variables
    @name = name
    @calories = calories
    if name.empty?
      raise(ArgumentError, "name must not be empty.")
    end
  end

  def name=(name)
    if name.empty?
      raise(ArgumentError, "name must not be empty.")
    end
    @name = name
  end

  def calories=(calories)
    @calories = calories
  end

  def name
      @name
  end

  def calories
      @calories
  end

  def healthy?
    if calories < 200
      return true
    else
      return false
    end
  end
  def delicious?
    return true
  end
end

class JellyBean < Dessert
  def initialize(flavor)
    @flavor = flavor
    @calories = 5
    @name = flavor + " jelly bean"
  end

  def flavor=(flavor)
    if flavor.empty?
      raise(ArgumentError, "flavor must not be empty.")
    end
    @flavor = flavor
  end

  def flavor
    @flavor
  end

  def delicious?
    if flavor == "licorice"
      return false
    else
      return true
    end
  end
end
