module FunWithStrings
  def palindrome?
    string = self.gsub(/[^0-9a-z]/i, '').downcase
    if string.length < 2
      return true
    elsif string == string.reverse
      return true
    else
      startstring = ""
      endstring = ""
      middle = ""
      remainingletters = ""
      string.split("").each do |letter|
        if string.count(letter) == 2
          if startstring.index(letter) == -1
            startstring = letter + startstring
            endstring = endstring + letter
          end
        elsif string.count(letter) == 1
          middle = middle + letter
        else
          remainingletters = remainingletters + letter
        end
      end
      if middle.length > 1
        return false
      elsif remainingletters.length > 0
        return false
      else
        return true
      end
    end
  end
  def count_words
    end_hash = {}
    string = self.gsub(/[^0-9a-z ]/i, '').downcase
    string.split(" ").each do |word|
      if end_hash.key?(word)
        end_hash[word] += 1
      else
        end_hash[word] = 1
      end
    end
    return end_hash
  end
  def anagram_groups
    end_array = []
    string = self.gsub(/[^0-9a-z ]/i, '').downcase
    array = string.split(" ")
    array2 = []
    array.each do |word|
      array2 << word.chars.sort.join
    end

    i = 0
    while i < array2.length
      end_array[i] = [array[i]] 
      k = array2.length - 1
      while k > i
        if array2[i] == array2[k]
          end_array[i] << array[k]
        end
        k -= 1
      end
      i += 1
    end
    return end_array
  end
end

# make all the above functions available as instance methods on Strings:

class String
  include FunWithStrings
end
