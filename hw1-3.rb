class BookInStock
    def initialize(isbn, price)  
        # Instance variables  
        @isbn = isbn
        @price = price
        if isbn.empty?
            raise(ArgumentError, "ISBN must not be empty.")
        end
        if price <= 0
            raise(ArgumentError, "Price must be greater than zero.")
        end
    end

    def isbn=(isbn)
        if isbn.empty?
            raise(ArgumentError, "ISBN must not be empty.")
        end
        @isbn = isbn
    end

    def price=(price)
        if price <= 0
            raise(ArgumentError, "Price must be greater than zero.")
        end
        @price = price
    end

    def isbn
        @isbn
    end

    def price
        @price
    end

    def price_as_string()
        return "$%.2f" % self.price
    end 
end
