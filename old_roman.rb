# Old-school Roman numerals. In the early days of Roman numerals, the Romans didn’t bother with any of this new-fangled subtraction “IX” nonsense. No sir, it was straight addition, biggest to littlest—so 9 was written “VIIII,” and so on. Write a method that when passed an integer between 1 and 3000 (or so) returns a string containing the proper old- school Roman numeral. In other words, old_roman_numeral 4 should return 'IIII'. Make sure to test your method on a bunch of different numbers. Hint: Use the integer division and modulus methods on page 32.
# For reference, these are the values of the letters used:
# I = 1 V = 5 X = 10 L = 50 C = 100 D = 500 M = 1000
def old_roman(num)
	ms = num / 1000
	roman = ""
	if ms > 0
		roman = "M"*ms
		num = num - (ms*1000)
	end
	ds = num / 500
	if ds > 0
		roman = roman + "D"
		num = num - 500
	end
	cs = num / 100
	if cs > 0
		roman = roman + ("C"*cs)
		num = num - (cs*100)
	end
	ls = num / 50
	if ls > 0
		roman = roman + "L"
		num = num - 50
	end
	xs = num / 10
	if xs > 0
		roman = roman + ("X"*xs)
		num = num - (xs*10)
	end
	vs = num / 5
	if vs > 0
		roman = roman + "V"
		num = num - 5
	end
	roman = roman + "I"*num
	return roman
end

# Now do the same with modern roman numerals
def roman_numeral(num)
	ms = num / 1000
	roman = ""
	if ms > 0
		roman = "M"*ms
		num = num - (ms*1000)
	end
	if num > 800
# 	if (num / 100 ) > 5
# 		roman = (10-(num / 100))
	ds = num / 500
	if ds > 0
		roman = roman + "D"
		num = num - 500
	end
	cs = num / 100
	if cs > 0
		roman = roman + ("C"*cs)
		num = num - (cs*100)
	end
	ls = num / 50
	if ls > 0
		roman = roman + "L"
		num = num - 50
	end
	xs = num / 10
	if xs > 0
		roman = roman + ("X"*xs)
		num = num - (xs*10)
	end
	vs = num / 5
	if vs > 0
		roman = roman + "V"
		num = num - 5
	end
	roman = roman + "I"*num
	return roman
end

puts old_roman(3)
puts old_roman(8)
puts old_roman(25)
puts old_roman(2854)
puts old_roman(682)